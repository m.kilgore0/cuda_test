#include <stdio.h>
#include <stdlib.h>

#include "ktiming.h"
#include "getoptions.h"

#include "ktiming.c"
#include "getoptions.c"

#ifndef TIMING_COUNT
#define TIMING_COUNT 1
#endif

#ifndef THRESHOLD
#define THRESHOLD 16
#endif

#define CHECK_RESULT 1

#if !defined(CPU_THRESHOLD) || (CPU_THRESHOLD <= 0)
#define CPU_THRESHOLD THRESHOLD
#endif

#if !defined(GPU_THRESHOLD) || (GPU_THRESHOLD <= 0)
#define GPU_THRESHOLD THRESHOLD
#endif

#define TRUE 1
#define FALSE 0

unsigned int randomSeed = 1;

__global__ void gpu_matrix_mult(int *C, const int *A, const int *B, int n, int length) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int loc = index; loc < length * length; loc += stride) {
    int i = loc / length;
    int j = loc % length;
    int sum = 0;
    for(int k = 0; k < length; k++) {
      sum += A[i * n + k] * B[k * n + j];
    }
    C[i * n + j] += sum;
  }
}

void cpu_matrix_mult(int *C, const int *A, const int *B, int n, int length) {
  for (int i = 0; i < length; i++)
    for (int j = 0; j < length; j++)
      for (int k = 0; k < length; k++)
	C[i*n+j] += A[i*n+k] * B[k*n+j];
}

static void mm_dac(int *C, const int *A, const int *B, int n, int length) {

  if(length <= GPU_THRESHOLD) {
    // Use a loop for small matrices
    gpu_matrix_mult<<<1, 256>>>(C, A, B, n, length);

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();
    return;
  }

  // Partition the matrices
  int mid = length >> 1;

  int *C00 = C;
  int *C01 = C + mid;
  int *C10 = C + n*mid;
  int *C11 = C + n*mid + mid;

  int const *A00 = A;
  int const *A01 = A + mid;
  int const *A10 = A + n*mid;
  int const *A11 = A + n*mid + mid;

  int const *B00 = B;
  int const *B01 = B + mid;
  int const *B10 = B + n*mid;
  int const *B11 = B + n*mid + mid;

  mm_dac(C00, A00, B00, n, mid);
  mm_dac(C01, A00, B01, n, mid);
  mm_dac(C10, A10, B00, n, mid);
  mm_dac(C11, A10, B01, n, mid);

  mm_dac(C00, A01, B10, n, mid);
  mm_dac(C01, A01, B11, n, mid);
  mm_dac(C10, A11, B10, n, mid);
  mm_dac(C11, A11, B11, n, mid);
}

static void mm_dac_serial(int *C, const int *A, const int *B, int n, int length) {

  if(length <= CPU_THRESHOLD) {
    // Use a loop for small matrices
    cpu_matrix_mult(C, A, B, n, length);
    return;
  }

  // Partition the matrices
  int mid = length >> 1;

  int *C00 = C;
  int *C01 = C + mid;
  int *C10 = C + n*mid;
  int *C11 = C + n*mid + mid;

  int const *A00 = A;
  int const *A01 = A + mid;
  int const *A10 = A + n*mid;
  int const *A11 = A + n*mid + mid;

  int const *B00 = B;
  int const *B01 = B + mid;
  int const *B10 = B + n*mid;
  int const *B11 = B + n*mid + mid;

  mm_dac_serial(C00, A00, B00, n, mid);
  mm_dac_serial(C01, A00, B01, n, mid);
  mm_dac_serial(C10, A10, B00, n, mid);
  mm_dac_serial(C11, A10, B01, n, mid);

  mm_dac_serial(C00, A01, B10, n, mid);
  mm_dac_serial(C01, A01, B11, n, mid);
  mm_dac_serial(C10, A11, B10, n, mid);
  mm_dac_serial(C11, A11, B11, n, mid);
}

static void print_matrix(int * A, int n) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++)
      printf("%d\t", A[i*n+j]);
    printf("\n");
  }
  printf("\n");
}

static void rand_matrix(int *dest, int n) {
  for(int i = 0; i < n*n; ++i)
    dest[i] = rand_r(&randomSeed) & 0xff;
}

static void zero_matrix(int *dest, int n) {
  for(int i = 0; i < n*n; ++i)
    dest[i] = 0;
}

#if CHECK_RESULT
static int are_equal_matrices(const int *a, const int *b, int n) {
  for(int i = 0; i < n*n; ++i)
    if(a[i] != b[i])
      return FALSE;
  return TRUE;
}
#endif

static void test_mm(int n, int check) {
  clockmark_t begin, end;
  uint64_t running_time[TIMING_COUNT];
  int ret = -1;

  fprintf(stderr, "GPU_THRESHOLD - %d\n", GPU_THRESHOLD);
  fprintf(stderr, "CPU_THRESHOLD - %d\n", CPU_THRESHOLD);

  int *A = 0;
  ret = cudaMallocManaged(&A, sizeof(int)*(n*n));
  fprintf(stderr, "cudaMallocManaged - %d\n", ret);
  int *B = 0;
  ret = cudaMallocManaged(&B, sizeof(int)*(n*n));
  fprintf(stderr, "cudaMallocManaged - %d\n", ret);
  int *C = 0;
  ret = cudaMallocManaged(&C, sizeof(int)*(n*n));
  fprintf(stderr, "cudaMallocManaged - %d\n", ret);

  rand_matrix(A, n);
  rand_matrix(B, n);

  //print_matrix(A, n);
  //print_matrix(B, n);

  for(int i = 0; i < TIMING_COUNT; i++) {
    zero_matrix(C, n);
    begin = ktiming_getmark();
    mm_dac(C, A, B, n, n);
    end = ktiming_getmark();
    //print_matrix(C, n);
    running_time[i] = ktiming_diff_usec(&begin, &end);
  }
  print_runtime(running_time, TIMING_COUNT);

  if(check) {
    fprintf(stderr, "Checking result ...\n");
    int * Cs = 0;
    ret = cudaMallocManaged(&Cs, sizeof(int) * (n*n));
    fprintf(stderr, "cudaMallocManaged - %d\n", ret);
    for(int i = 0; i < TIMING_COUNT; i++) {
      zero_matrix(Cs, n);
      begin = ktiming_getmark();
      mm_dac_serial(Cs, A, B, n, n);
      end = ktiming_getmark();
      //print_matrix(Cs, n);
      running_time[i] = ktiming_diff_usec(&begin, &end);
    }
    print_runtime(running_time, TIMING_COUNT);
    
    if(!are_equal_matrices(C, Cs, n)) {
      fprintf(stderr, "MM_dac test FAILED.\n");
    } else {
      fprintf(stderr, "MM_dac test passed.\n");
    }
    cudaFree(Cs);
  }

  cudaFree(C);
  cudaFree(B);
  cudaFree(A);
}

// return true iff n = 2^k (or 0).
static int is_power_of_2(int n) {
  return (n & (n-1)) == 0;
}


const char *specifiers[] = {"-n", "-c", "-h", 0};
int opt_types[] = {LONGARG, BOOLARG, BOOLARG, 0};

int main(int argc, char *argv[]) {

  long size;
  int help, check;

  /* standard benchmark options */
  size = 1024;
  check = 0;
  help = 0;

  get_options(argc, argv, specifiers, opt_types, &size, &check, &help);

  if(help) {
    fprintf(stderr, "Usage: mm_dac [cilk options] -n <size> [-c|-h]\n");
    fprintf(stderr, "   when -c is set, check result against sequential MM (slow).\n");
    fprintf(stderr, "   when -h is set, print this message and quit.\n");
    exit(0);
  }

  if(!is_power_of_2(size)) {
    fprintf(stderr, "Input size must be a power of 2 \n");
    exit(1);
  }
  test_mm(size, check);

  return 0;
}
