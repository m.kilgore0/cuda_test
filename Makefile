CC=nvcc

TESTS   = mm_dac

TIMING_COUNT := 1
THRESHOLD := 16
OPT := 0

OPTIONS = -g -O$(OPT) -DTIMING_COUNT=$(TIMING_COUNT) -DTHRESHOLD=$(THRESHOLD)

ifdef GPU
OPTIONS += -DGPU_THRESHOLD=$(GPU)
endif

ifdef CPU
OPTIONS += -DCPU_THRESHOLD=$(CPU)
endif

.PHONY: all check clean

all: $(TESTS)

$(TESTS): %: %.o #ktiming.o getoptions.o
	$(CC) $^ -o $@

mm_dac.o: mm_dac.cu
	$(CC) -c $(OPTIONS) -o $@ $<

%.o: %.c
	$(CC) -c $(OPTIONS) -o $@ $<

check:
	make clean; make TIMING_COUNT=5 >& /dev/null
	./mm_dac -n 1024 -c

clean:
	rm -f *.o *~ $(TESTS) core.*
